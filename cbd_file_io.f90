module cbd_file_io

    use iso_fortran_env, only: int64, real32, real64, error_unit

    implicit none
    private
    public :: read_cbd_file, write_cbd_file

    character, parameter :: CBD_MAGIC_NUMBER(2) = (/ char(z'CB'), char(z'DF') /)
    character, parameter :: CBD_VERSION = char(z'01')

contains

    subroutine write_cbd_file(filename, data3d, x1, x2, x3, xmin, xmax, kind)

        character(*), intent(in) :: filename
        real(real64), dimension(:,:,:), intent(in) :: data3d
        real(real64), dimension(size(data3d,1)), intent(in) :: x1
        real(real64), dimension(size(data3d,2)), intent(in) :: x2
        real(real64), dimension(size(data3d,3)), intent(in) :: x3
        real(real64), dimension(3), intent(in) :: xmin
        real(real64), dimension(3), intent(in) :: xmax
        integer, intent(in), optional :: kind

        integer :: io_unit
        character, dimension(8) :: id = char(0) ! initialize to zero
        integer(int64), dimension(3) :: N
        real(real32), dimension(:,:,:), allocatable :: data3d_file

        ! set bytes for magic number and version
        id(1:2) = CBD_MAGIC_NUMBER
        id(3) = CBD_VERSION

        ! set byte for data precision
        if (present(kind) .and. kind == real32) then
            id(8) = char(4)
        else if (present(kind) .and. kind /= real64) then
            write(error_unit, *) 'Unsupported data type: &
                &Only 32-bit and 64-bit precision is supported.'
            error stop
        else
            id(8) = char(8)
        end if

        ! read dimensions of data, converting to Int64
        N = shape(data3d)

        open(newunit=io_unit, file=filename, status='NEW', &
            action='WRITE', access='STREAM')

        ! write metadata
        write(io_unit) id, N, xmin, xmax, x1, x2, x3

        ! write data, converting to different precision if necessary
        if (present(kind) .and. kind == real32) then
            allocate(data3d_file(N(1), N(2), N(3)))
            data3d_file = real(data3d, kind=real32)
            write(io_unit) data3d_file
            deallocate(data3d_file)
        else
            write(io_unit) data3d
        end if

        close(io_unit)

    end subroutine

    subroutine read_cbd_file(filename, data3d, x1, x2, x3, xmin, xmax)

        character(*), intent(in) :: filename
        real(real64), dimension(:,:,:), allocatable, intent(out) :: data3d
        real(real64), dimension(:), allocatable, intent(out) :: x1
        real(real64), dimension(:), allocatable, intent(out) :: x2
        real(real64), dimension(:), allocatable, intent(out) :: x3
        real(real64), dimension(3), intent(out) :: xmin
        real(real64), dimension(3), intent(out) :: xmax

        integer :: io_unit
        character, dimension(8) :: id = char(0) ! initialize to zero
        integer(int64), dimension(3) :: N
        real(real32), dimension(:,:,:), allocatable :: data3d_r32

        open(newunit=io_unit, file=filename, status='OLD', &
            action='READ', access='STREAM')

        ! read id and compare bytes for magic number, version, and precision
        read(io_unit) id
        if (.not. all(id(1:2) == CBD_MAGIC_NUMBER)) then
            write(error_unit, *) 'Not a CBD file (magic number not matching).'
            close(io_unit)
            error stop
        else if (id(3) /= CBD_VERSION) then
            write(error_unit, *) 'Unsupported version of CBD format.'
            close(io_unit)
            error stop
        else if (.not. (id(8) == char(4) .or. id(8) == char(8))) then
            write(error_unit, *) 'Unsupported data type: &
                &Only 32-bit and 64-bit precision is supported.'
            close(io_unit)
            error stop
        end if

        ! read dimensions of data and domain
        read(io_unit) N, xmin, xmax

        ! read arrays of coordinates
        allocate(x1(N(1)))
        allocate(x2(N(2)))
        allocate(x3(N(3)))
        read(io_unit) x1, x2, x3

        ! read data, converting to different precision if necessary
        allocate(data3d(N(1), N(2), N(3)))
        if (id(8) == char(8)) then
            read(io_unit) data3d
        else
            allocate(data3d_r32(N(1), N(2), N(3)))
            read(io_unit) data3d_r32
            data3d = real(data3d_r32, kind=real64)
            deallocate(data3d_r32)
        end if

        close(io_unit)

    end subroutine

end module
