function [N,xmin,ymin,zmin,xmax,ymax,zmax,x1,x2,x3,data] = read_cbd(fin)
    
    fid = fopen(fin,'r');
    
    magic_number = fread(fid, 1, 'uint64=>uint64');
  
    if (magic_number == uint64(288230376151834571))
        data_type = 'float32';
    elseif (magic_number == uint64(576460752303546315))
        data_type = 'float64';
    else
        disp('Invalid ID');
        return;
    end
  
    N(1) = fread(fid, 1, 'int64'); % number of nodes along x
    N(2) = fread(fid, 1, 'int64'); % number of nodes along y
    N(3) = fread(fid, 1, 'int64'); % number of nodes along z
    
    xmin = fread(fid, 1, 'float64'); % location of the first node along x
    ymin = fread(fid, 1, 'float64'); % location of the first node along y
    zmin = fread(fid, 1, 'float64'); % location of the first node along z
    
    xmax = fread(fid, 1, 'float64'); % location of the last node along x
    ymax = fread(fid, 1, 'float64'); % location of the last node along y
    zmax = fread(fid, 1, 'float64'); % location of the last node along z
    
    x1   = fread(fid, N(1),'float64'); % vector with locations of nodes along x
    x2   = fread(fid, N(2),'float64'); % vector with locations of nodes along y
    x3   = fread(fid, N(3),'float64'); % vector with locations of nodes along z
    
    data = fread(fid,N(1)*N(2)*N(3),data_type);
    data = reshape(data,N(1),N(2),N(3)); % matrix with values of the field f at each node

    fclose(fid);
  
end
