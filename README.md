# Cartesian Binary Data

*The Cartesian Binary Data format is a simple file format to save three-dimensional data on a Cartesian grid. It is meant to be used for simulation output (snapshots and restart files) as well as for storing post-processing results.*

**Extension: `.cbd`**

| Field | Size | Description |
| :----: | :--: | ----------- |
| `ID` | 8 bytes | The first two bytes are the [magic number](https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicator) `CB` `DF` used to identify the file format. The third byte is the version of the format and should be set to `01`. The last byte is used to specify the variant of the format. It should be set to `04` if the data is stored in single-precision floats or `08` for double-precision. The full identifier for a file storing 64-bit data would therefore be `CB` `DF` `01` `00` `00` `00` `00` `08`. The unused bytes of the identifier can be used for additional variants of the format in the future and should be set to zero. |
| `N1`, `N2`, `N3` | 3 × `UInt64` | The number of values along the first, second, and third spatial direction. |
| `XMIN` | 3 × `Float64` | The start of the domain along the first, second, and third spatial direction. |
| `XMAX` | 3 × `Float64` | The end of the domain along the first, second, and third spatial direction. |
| `X1` | `N1` × `Float64` | The spatial position of the values along the first spatial direction. |
| `X2` | `N2` × `Float64` | The spatial position of the values along the second spatial direction. |
| `X3` | `N3` × `Float64` | The spatial position of the values along the third spatial direction. |
| `DATA` | `N1` × `N2` × `N3` × `Float32` or `Float64` | Three-dimensional data with the entries along the first dimension being adjacent (fastest-varying index), then the second dimension, then the third (Fortran memory order). |

The values are stored as unsigned integers or [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754) binary floating-point values in [little-endian memory order](https://en.wikipedia.org/wiki/Endianness), i.e. the default unsigned integers and single/double precision numbers of a system with Intel x86 processors.

## Writing the ID

On little-endian systems (most current computers), the least significant byte is
stored first. This means that if the ID is expressed as a 64-bit value in the
code, the byte order is reversed. The hexadecimal literal for the ID is
`0x040000000001dfcb` for single-precision data and `0x080000000001dfcb` for
double-precision. This corresponds to the integer values `288230376151834571`
for single-precision and `576460752303546315` for double-precision. Since the
first bit is zero, the numerical value is the same for signed and unsigned integers.

To check whether a file has the correct ID, you can show its first eight bytes
with the command `od -N8 -tx1 -An FILE.cbd`. The output should start with `cb df 01`
and end with `04` or `08` with zeros in-between.

## Example Usage

Here are some quick example of how files in the CBD format can be read and
written in a few common languages. For more complete examples with better error
handling please refer to the separate files `cbd_file_io.*` and the usage
examples in the `examples` folder.

### Reading CBD-Files

Julia (with [memory mapping](https://en.wikipedia.org/wiki/Memory-mapped_file))
```julia
import Mmap
open(filename, "r") do f
    id = read(f, UInt64)
    T = id == 288230376151834571 ? Float32 : id == 576460752303546315 ? Float64 :
            error("Invalid ID")
    N = Tuple(read(f, UInt64) for i=1:3)
    xmin = Tuple(read(f, Float64) for i=1:3)
    xmax = Tuple(read(f, Float64) for i=1:3)
    x1, x2, x3 = Tuple(read!(f, zeros(Float64, n)) for n=N)
    data = Mmap.mmap(f, Array{T,3}, N)
end
```

Python with NumPy
```python
with open(filename, "rb") as f:
    identifier = f.read(8)
    T = np.float32 if identifier[-1:] == bytearray([4]) else np.float64
    N = tuple(np.fromfile(f, dtype=np.int64, count=3))
    xmin = tuple(np.fromfile(f, dtype=np.float64, count=3))
    xmax = tuple(np.fromfile(f, dtype=np.float64, count=3))
    x1, x2, x3 = (np.fromfile(f, dtype=np.float64, count=n) for n in N)
np.memmap(filename, dtype=T, shape=N, offset=8*(10+np.sum(N)), order="F", mode="r")
```

Fortran
```fortran
open(newunit=io_unit, file=filename, status='OLD', &
    action='READ', access='STREAM')
read(io_unit) id
if (id == 288230376151834571) then
    rp_data = real32
else if (id == 576460752303546315) then
    rp_data = real64
else
    write(error_unit, *) 'Invalid ID'
    close(io_unit)
    error stop
end if
read(io_unit) N, xmin, xmax
allocate(x1(N(1)))
allocate(x2(N(2)))
allocate(x3(N(3)))
read(io_unit) x1, x2, x3
allocate(data3d(N(1), N(2), N(3)))
if (rp_data == real32) then
    allocate(data3d_r32(N(1), N(2), N(3)))
    read(io_unit) data3d_r32
    data3d = real(data3d_r32, kind=real64)
    deallocate(data3d_r32)
else
    read(io_unit) data3d
end if
close(io_unit)
```

Matlab
```matlab
function [N,xmin,ymin,zmin,xmax,ymax,zmax,x1,x2,x3,data] = read_cdb(fin)
    
    fid = fopen(fin,'r');
    magic_number = fread(fid, 1, 'int64');

    if (magic_number == uint64(288230376151834571))
        data_type = 'float32';
    elseif (magic_number == uint64(576460752303546315))
        data_type = 'float64';
    end
    
    N(1) = fread(fid, 1, 'int64'); % number of nodes along x
    N(2) = fread(fid, 1, 'int64'); % number of nodes along y
    N(3) = fread(fid, 1, 'int64'); % number of nodes along z
    xmin = fread(fid, 1, 'float64'); % location of the first node along x
    ymin = fread(fid, 1, 'float64'); % location of the first node along y
    zmin = fread(fid, 1, 'float64'); % location of the first node along z
    xmax = fread(fid, 1, 'float64'); % location of the last node along x
    ymax = fread(fid, 1, 'float64'); % location of the last node along y
    zmax = fread(fid, 1, 'float64'); % location of the last node along z
    x1   = fread(fid, N(1),'float64'); % vector with locations of nodes along x
    x2   = fread(fid, N(2),'float64'); % vector with locations of nodes along y
    x3   = fread(fid, N(3),'float64'); % vector with locations of nodes along z
    data = fread(fid,N(1)*N(2)*N(3),data_type);
    data = reshape(data,N(1),N(2),N(3)); % matrix with values of the field f at each node

    fclose(fid);
  
end
```

### Writing CBD-Files

Julia
```julia
id = T == Float32 ? 288230376151834571 :
     T == Float64 ? 576460752303546315 :
     error("Invalid data type")
open(filename, "w") do f
    write(f, UInt64(id))
    write(f, convert.(UInt64, size(data))...)
    write(f, convert.(Float64, xmin)...)
    write(f, convert.(Float64, xmax)...)
    for x = (x1, x2, x3)
        write(f, convert(Array{Float64,1}, x))
    end
    write(f, convert(Array{T,3}, data))
end
```

Python with NumPy
```python
id = 288230376151834571 if T == np.float32 else \
     576460752303546315 if T == np.float64 else \
     raise ValueError("Invalid data type")
with open(filename, "wb") as f:
    np.asarray(id, dtype=np.uint64).tofile(f)
    np.asarray(data.shape, dtype=np.uint64).tofile(f)
    np.asarray(xmin, dtype=np.float64).tofile(f)
    np.asarray(xmax, dtype=np.float64).tofile(f)
    for x in (x1, x2, x3):
        np.asarray(x, dtype=np.float64).tofile(f)
    data.astype(T).flatten(order="F").tofile(f)
```

Fortran
```fortran
if (rp_data == real32) then
    id = 288230376151834571
else if (rp_data == real64) then
    id = 576460752303546315
else
    write(error_unit, *) 'Invalid data type'
    error stop
end if
open(newunit=io_unit, file=filename, status='NEW', &
    action='WRITE', access='STREAM')
write(io_unit) id, N, xmin, xmax, x1, x2, x3
if (rp_data == real32) then
    allocate(data3d_file(N(1), N(2), N(3)))
    data3d_file = real(data3d, kind=real32)
    write(io_unit) data3d_file
    deallocate(data3d_file)
else
    write(io_unit) data3d
end if
close(io_unit)
```

## Design Considerations

### Name

The file format stores *binary* data on a *Cartesian* grid, which can be irregularly spaced. The abbreviation *CBD* is does not appear to be used for a file format yet, which avoids confusion with other files and software, although it is a well-known abbreviation for “central business district” and “Cannabidiol”.

Alternatives would be Cartesian Binary Format (`.cbf` is already used for some other file formats, although not very popular ones) or Binary Cartesian Data (`.bcd` is also in use already).

### Identifier

Since the letters of the abbreviation are also hexadecimal numbers, we can directly use the name of the format as magic number. To get an even number of bytes we add an extra `F`, which could stand for “file” or “format”.

### Precision and Number Types

Since 64-bit numbers are the default on all modern systems, we use those as much as possible. For the main data, we also allow storing the values in single precision, since some of the generated data sets can be quite large. The integer values are stored as unsigned integers since array dimensions can only be positive. In practice, this only matters for the identifier, since signed and unsigned integers are identical for relatively small values.

One option would have been to also support writing complex numbers so we can store frequency domain data. This would make it possible for spectral codes to directly save frequency domain information without transforming back to the physical domain. However, the interpretation of these frequencies depends on the implementation of the simulation, and codes with different numerics would produce different data. Furthermore, the transformation to physical domain data is lossless and can be inverted when the files are read again. Therefore, we always write physical domain data and avoid dealing with complex numbers and their respective meaning.

### Domain Size

It is not clear whether it really makes sense to store the domain size. In a way, the values of `X1`, `X2`, and `X3` also contain the spatial dimensions and having a separate value for the domain size makes it possible to produce files with invalid domain sizes.

In practice, we often don’t store values all the way to the border of the domain and it can be useful to know how far out the values have any meaning, e.g. when plotting the data or when extrapolating the data.

We could also consider saving the domain as an interval, i.e. also storing the origin `O1`, `O2`, and `O3` and not only the size. In most cases these values would all be zero, but maybe it could sometimes be useful to have a bit more generality.

### Grid Point Locations

Saving the locations of grid points in each direction simplifies the handling of staggered grids, since we do not have to manage any extra information of which field is on which set of grid points. Furthermore, we can easily support grids that are stretched in one direction this way. The drawback is that we store some extra data. Furthermore, most post-processing tools will have to “know” about the staggered grid and the location of each variable anyway, since the numerical methods rely on the exact spatial arrangement and the tools are not equipped to deal with arbitrary grid point locations. In many use cases, such as plotting or loading data from a simulation with different resolution, it can be very useful to have the grid point locations there alongside the data. Overall, it seems to be worth writing these values to the file.

### Metadata

We could consider including more metadata within the file, such as the simulation time the file corresponds to. However, it is hard to decide which parameters are important enough to be included and there will always be extra parameters that are not included. Therefore, we keep the values inside the file minimal and rely on other methods (file names, separate files with parameters, documenting our work) to manage this data.

### Multiple Fields

It might be tempting to include several fields in one file, for example to make it possible to use a single file for restarts. However, it is hard to do this in a way that is both general enough and not overly complex. While a standard channel flow simulation only requires three velocities for a restart, we already get more fields if we add temperature or other scalars to the simulation. Even then, we would still want to save other output in our format (e.g. the wall stresses from an immersed boundary). Therefore, we would have to keep track of how many fields there are inside a file, either within the file format or with external documentation. It seems much simpler to me to simply write and read three separate files for a restart than to combine an arbitrary number of fields into one file.

### Multiple Time Steps

Another option worth considering is writing multiple time steps to the same file. A running simulation could then just reopen the existing files and append a new time step. The advantage of this is that fewer files are needed, so navigating the output folders might be easier and fewer inodes are used for the output. The files would also be less redundant, since grid point locations are the same for the whole time series. The drawbacks are that a crash during a later write could corrupt earlier output data, and it is difficult to download, move, or save individual time steps or a subset thereof. Furthermore, if the file format should support appending new time steps to an existing file, the format would have to become quite a lot more complex, since we cannot just save a list of the time steps in the header as this list has to grow later on. It is therefore probably not worth supporting multiple time steps in a single file.
